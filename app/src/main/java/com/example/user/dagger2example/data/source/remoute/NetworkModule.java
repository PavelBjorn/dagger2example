package com.example.user.dagger2example.data.source.remoute;

import dagger.Module;
import dagger.Provides;

/**
 * Created by User on 13.09.2017.
 */
@Module
public class NetworkModule {

    @Provides
    NetworkProvider getNetworkProvider(){
        return new NetworkProvider();
    }
}
