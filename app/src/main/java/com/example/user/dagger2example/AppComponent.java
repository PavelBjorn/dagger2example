package com.example.user.dagger2example;

import com.example.user.dagger2example.data.source.local.DbModule;
import com.example.user.dagger2example.data.source.local.DbProvider;
import com.example.user.dagger2example.data.source.remoute.NetworkModule;
import com.example.user.dagger2example.data.source.remoute.NetworkProvider;
import com.example.user.dagger2example.presentation.avtivity.MainActivity;

import dagger.Component;

/**
 * Created by User on 13.09.2017.
 */
@Component(modules = {DbModule.class, NetworkModule.class})
public interface AppComponent {
    void injectsMainActivity(MainActivity mainActivity);
}
