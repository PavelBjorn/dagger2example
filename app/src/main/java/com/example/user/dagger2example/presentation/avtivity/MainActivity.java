package com.example.user.dagger2example.presentation.avtivity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.example.user.dagger2example.App;
import com.example.user.dagger2example.R;
import com.example.user.dagger2example.data.source.local.DbProvider;
import com.example.user.dagger2example.data.source.remoute.NetworkProvider;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Inject
    NetworkProvider mNetworkProvider;

    @Inject
    DbProvider mDbProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        App.getAppComponent().injectsMainActivity(this);
        Log.d(TAG, "mNetworkProvider = [" + mNetworkProvider + "]" + " mDbProvider = [" + mDbProvider + "]");
    }

}
