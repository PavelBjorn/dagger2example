package com.example.user.dagger2example.data.source.local;

import dagger.Module;
import dagger.Provides;

/**
 * Created by User on 13.09.2017.
 */
@Module
public class DbModule {

    @Provides
    DbProvider getDbProvider() {
        return new DbProvider();
    }
}
